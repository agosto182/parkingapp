# Parking App

App made in Nodejs with express and sequelize.

## Installation (with docker)

Copy the example environment:
```
$ cp .env.example .env
```

Run docker compose command:
```
$ npm run docker:run
```

The application will run on the port 3000.

## Manual Installation

Install dependencies:
```
$ npm install
```

Copy and modify your environment:
```
$ cp .env.example .env
```

Start the app:
```
$ npm start
```

The application will run on the port 3000.

## Testing

To test the application run:

```
$ npm run test
```

## Linting code

To run lint with eslint:

```
$ npm run lint
```

import express from 'express';
import jwt from 'jsonwebtoken';
import router from '../src/router';
import { Sequelize } from 'sequelize-typescript';
import { User } from '../src/models/user.model';
import { Parking } from '../src/models/parking.model';
import { Ticket } from '../src/models/ticket.model';
import { getConfig } from '../src/config';

export const generateApp = async () => {
  // create fake app
  const app = express();
  app.use(express.urlencoded());
  app.use(express.json());

  // conect dabatase
  const database = new Sequelize({
    dialect: 'sqlite',
    storage: ':memory:',
    models: [User, Parking, Ticket],
    logging: false
  });

  await database.sync({ force: true });

  app.use('/api', router());

  return app;
};

export const generateToken = () => {
  const secret = getConfig('JWT_TOKEN', 'secret') as string;

  return jwt.sign({ id: 1, email: 'fake_user@mail.com' }, secret);
};

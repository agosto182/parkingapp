import { describe, it, beforeAll, expect } from '@jest/globals';
import { generateApp, generateToken } from '../../../helper';
import request from 'supertest';
import { Application } from 'express';

describe('Integration test for checkIn', () => {
  let app: Application;
  let token: string;
  const parking = {
    name: 'test 1',
    spots: 50,
    contact: 'test',
    parkingType: 'public'
  };
  const userType = 'visitor';

  beforeAll(async () => {
    app = await generateApp();
    token = generateToken();
    return true;
  });

  it('POST /api/checkIn should return a ticket', async () => {
    const {
      body: parkingCreated
    } = await request(app).post('/api/parking').set({ Authorization: token }).send(parking);

    const res = await request(app).post('/api/checkIn').send({
      parkingId: parkingCreated.id,
      userType
    });

    expect(res.statusCode).toBe(201);
    expect(res.body.status).toEqual('success');
  });

  it('POST /api/checkIn should check in private parking as corporate', async () => {
    const { body: parkingCreated } = await request(app).post('/api/parking').set({ Authorization: token }).send({
      ...parking,
      name: 'test private',
      parkingType: 'private'
    });

    const res = await request(app).post('/api/checkIn').send({
      parkingId: parkingCreated.id,
      userType: 'corporate'
    });

    expect(res.statusCode).toBe(201);
    expect(res.body.status).toEqual('success');
  });
});

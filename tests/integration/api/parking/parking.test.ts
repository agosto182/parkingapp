import { describe, it, beforeAll, expect } from '@jest/globals';
import { generateApp, generateToken } from '../../../helper';
import request from 'supertest';
import { Application } from 'express';

describe('Integration test for checkIn', () => {
  let app: Application;
  let token: string;
  const parking = {
    name: 'test 1',
    spots: 50,
    contact: 'test',
    parkingType: 'public'
  };

  beforeAll(async () => {
    app = await generateApp();
    token = generateToken();

    return true;
  });

  it('POST /api/parking sould create a parking', async () => {
    const res = await request(app).post('/api/parking').set({ Authorization: token }).send(parking);

    expect(res.statusCode).toBe(201);
    expect(res.body.name).toEqual(parking.name);
  });

  it('GET /api/parking return all the parking\'s', async () => {
    const res = await request(app).get('/api/parking').set({ Authorization: token });

    expect(res.statusCode).toBe(200);
    expect(res.body.count).toEqual(1);
  });
});

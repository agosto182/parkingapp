import { beforeAll, beforeEach, describe, expect, it, jest } from '@jest/globals';
import { CheckInService } from '../../../src/services/check-in.service';
import { TicketStore } from '../../../src/stores/ticket.store';
import { TicketInputInterface } from '../../../src/interfaces/ticket-input.interface';
import { ParkingStore } from '../../../src/stores/parking.store';

const MOCK_PARKING: any = {
  id: 1,
  name: 'test',
  spots: 50,
  contact: 'test'
};

const MOCK_TICKET_IMPLEMENTATION = (parkingId: number, userType: string) => {
  return Promise.resolve({
    id: 1,
    parkingId,
    userType
  } as any);
};

describe.only('Unit testing for checkIn service', () => {
  let checkInService: CheckInService;
  let mockTicketStore: any;
  let mockParkingStore: any;
  let mockDate: any;

  beforeAll(() => {
    checkInService = new CheckInService();

    mockTicketStore = jest.spyOn(TicketStore.prototype, 'create').mockImplementation(MOCK_TICKET_IMPLEMENTATION);
    mockParkingStore = jest.spyOn(ParkingStore.prototype, 'getById');
    mockDate = jest.spyOn(Date.prototype, 'getDay');
  });

  beforeEach(() => {
    mockTicketStore.mockClear();
    mockTicketStore.mockClear();
    mockDate.mockClear();
  });

  it('Should return ticket when parking is public and user is provider', async () => {
    mockParkingStore.mockResolvedValueOnce({
      ...MOCK_PARKING,
      parkingType: 'public'
    });
    const payload: TicketInputInterface = {
      parkingId: MOCK_PARKING.id,
      userType: 'visitor'
    };
    const response = await checkInService.checkIn(payload);

    expect(response).toHaveProperty('id');
    expect(mockParkingStore).toHaveBeenCalled();
    expect(mockTicketStore).toHaveBeenCalled();
  });

  it('Should return ticket when parking is private and user is corporate', async () => {
    mockDate.mockReturnValueOnce(1);
    mockParkingStore.mockResolvedValueOnce({
      ...MOCK_PARKING,
      parkingType: 'private'
    });
    const payload: TicketInputInterface = {
      parkingId: MOCK_PARKING.id,
      userType: 'corporate'
    };
    const response = await checkInService.checkIn(payload);

    expect(response).toHaveProperty('id');
    expect(mockParkingStore).toHaveBeenCalled();
    expect(mockTicketStore).toHaveBeenCalled();
  });

  it('Should return ticket when parking is courtesy and user is visitor', async () => {
    mockDate.mockReturnValueOnce(0);
    mockParkingStore.mockResolvedValueOnce({
      ...MOCK_PARKING,
      parkingType: 'courtesy'
    });
    const payload: TicketInputInterface = {
      parkingId: MOCK_PARKING.id,
      userType: 'visitor'
    };
    const response = await checkInService.checkIn(payload);

    expect(response).toHaveProperty('id');
    expect(mockParkingStore).toHaveBeenCalled();
    expect(mockTicketStore).toHaveBeenCalled();
  });
});

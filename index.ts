import express from 'express';
import router from './src/router';
import { connectDatabase } from './src/database';
import { getConfig } from './src/config';
import * as dotenv from 'dotenv';

if (process.env.IGNORE_DOT_ENV !== undefined) {
  dotenv.config();
}

const app = express();
const configPort = getConfig('PORT', '3000') as string;
const port = parseInt(configPort);

connectDatabase();

app.use(express.urlencoded());
app.use(express.json());

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(port, function () {
  app.use('/api', router());
  console.log(`Example app listening on port ${port}!`);
});

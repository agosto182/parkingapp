import { UserStore } from '../stores/user.store';
import { UserInputInterface } from '../interfaces/user-input.interface';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { BadRequest } from '../errors';
import { getConfig } from '../config';

export class UserService {
  private secretJwt = getConfig('JWT_SECRET', 'secret') as string;
  private store = new UserStore();

  async signUpUser (userInput: UserInputInterface) {
    const { email, password } = userInput;

    const passwordEncrypt = await bcrypt.hash(password, 10);
    const user = await this.store.create(email, passwordEncrypt);

    return user;
  }

  async authUser (userInput: UserInputInterface) {
    const { email, password } = userInput;
    const user = await this.store.getByEmail(email);

    if (!bcrypt.compareSync(password, user.password)) {
      throw new BadRequest('InvalidCredentials');
    }

    const token = jwt.sign(user.toJSON(), this.secretJwt);

    return token;
  }
}

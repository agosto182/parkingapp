import { BadRequest } from '../errors';
import { ParkingInput } from '../interfaces/parking-input.interface';
import { SearchInterface } from '../interfaces/search.interface';
import { ParkingStore } from '../stores/parking.store';

export class ParkingService {
  private store = new ParkingStore();

  async createParking (input: ParkingInput) {
    const { name, spots, contact, parkingType } = input;

    const parking = await this.store.create(
      name,
      spots,
      contact,
      parkingType
    );

    return parking;
  }

  async getParking (id: number) {
    const parking = await this.store.getById(id);

    return parking;
  }

  async getParkings (payload: SearchInterface) {
    const { skip, limit, order } = payload;
    const parkings = await this.store.getPaginated(skip, limit, order);

    return parkings;
  }

  async updateParking (id: number, payload: Partial<ParkingInput>) {
    const parking = await this.store.getById(id);

    if (!parking) {
      throw new BadRequest('ParkingNotFound');
    }

    const contact = payload.contact || parking.contact;
    const spots = payload.spots || parking.spots;

    return this.store.update(id, contact, spots);
  }
}

import { BadRequest } from '../errors';
import { CheckInInterface } from '../interfaces/check-in.interface';
import { TicketInputInterface } from '../interfaces/ticket-input.interface';
import { ParkingStore } from '../stores/parking.store';
import { CheckInCourtesyService } from './check-in-courtesy.service';
import { CheckInPrivateService } from './check-in-private.service';
import { CheckInPublicService } from './check-in-public.service';

export class CheckInService implements CheckInInterface {
  private parkingStore = new ParkingStore();

  async checkIn (payload: TicketInputInterface) {
    const { parkingId } = payload;
    let check: CheckInInterface;

    const parking = await this.parkingStore.getById(parkingId);

    if (!parking) {
      throw new BadRequest('ParkingNotFound');
    }

    switch (parking.parkingType) {
      case 'courtesy':
        check = new CheckInCourtesyService();
        break;
      case 'private':
        check = new CheckInPrivateService();
        break;
      case 'public':
      default:
        check = new CheckInPublicService();
        break;
    }

    const ticket = await check.checkIn(payload);
    return ticket;
  }
}

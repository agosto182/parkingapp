import { CheckInInterface } from '../interfaces/check-in.interface';
import { TicketInputInterface } from '../interfaces/ticket-input.interface';
import { TicketStore } from '../stores/ticket.store';

export class CheckInPublicService implements CheckInInterface {
  ticketStore = new TicketStore();

  async checkIn (payload: TicketInputInterface) {
    const { userType, parkingId } = payload;
    const ticket = await this.ticketStore.create(parkingId, userType);

    return ticket;
  }
}

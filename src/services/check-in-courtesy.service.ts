import { BadRequest } from '../errors';
import { CheckInInterface } from '../interfaces/check-in.interface';
import { TicketInputInterface } from '../interfaces/ticket-input.interface';
import { CheckInPublicService } from './check-in-public.service';

export class CheckInCourtesyService extends CheckInPublicService implements CheckInInterface {
  async checkIn (payload: TicketInputInterface) {
    const { userType } = payload;
    const today = new Date().getDay();

    if (userType !== 'visitor') {
      throw new BadRequest('UserNotAllowed');
    }

    if ([6, 0].indexOf(today) === -1) {
      throw new BadRequest('ParkingIsClosed');
    }

    return await super.checkIn(payload);
  }
}

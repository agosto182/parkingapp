import { Router } from 'express';
import parkingController from './api/parking';
import checkInController from './api/checkIn';
import userController from './api/users';

export default function (app: Router = Router()) {
  app.use('/parking', parkingController());
  app.use('/checkIn', checkInController());
  app.use('/users', userController());

  return app;
}

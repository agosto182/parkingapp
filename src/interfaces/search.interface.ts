export interface SearchInterface {
  skip?: number,
  limit?: number,
  order?: string
}

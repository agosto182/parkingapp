export interface ParkingInput {
  name: string;
  spots: number;
  contact: string;
  parkingType: string;
}

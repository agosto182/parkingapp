import { Ticket } from '../models/ticket.model';
import { TicketInputInterface } from './ticket-input.interface';

export interface CheckInInterface {
  checkIn(payload: TicketInputInterface): Promise<Ticket>;
}

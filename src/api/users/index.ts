import { Router } from 'express';
import { UserService } from '../../services/user.service';
import { validateBody } from '../../middlewares/validate-body.middleware';
import { loginUser, registerUser } from './users';
import { userValidation } from '../../validations/user.validation';

export default () => {
  const userService = new UserService();
  const router = Router();

  router.post('/register', validateBody(userValidation), registerUser(userService));
  router.post('/login', validateBody(userValidation), loginUser(userService));

  return router;
};

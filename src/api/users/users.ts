import { Request, Response } from 'express';
import { UserService } from '../../services/user.service';
import { UserInputInterface } from '../../interfaces/user-input.interface';

export const registerUser = (userService: UserService) => async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const payload: UserInputInterface = {
    email: email as string,
    password: password as string
  };

  try {
    const user = await userService.signUpUser(payload);

    return res.status(200).send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
};

export const loginUser = (userService: UserService) => async (req: Request, res: Response) => {
  const { email, password } = req.body;

  const payload: UserInputInterface = {
    email: email as string,
    password: password as string
  };

  try {
    const token = await userService.authUser(payload);

    return res.status(200).send({ token });
  } catch (error) {
    return res.status(400).send(error);
  }
};

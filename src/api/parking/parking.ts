import { Request, Response } from 'express';
import { ParkingService } from '../../services/parking.service';
import { ParkingInput } from '../../interfaces/parking-input.interface';
import { SearchInterface } from '../../interfaces/search.interface';
import { BadRequest } from '../../errors';

export const postParking = (parkingService: ParkingService) => async (req: Request, res: Response) => {
  const { body } = req;
  const input: ParkingInput = {
    name: body.name,
    spots: body.spots,
    contact: body.contact,
    parkingType: body.parkingType
  };

  try {
    const parking = await parkingService.createParking(input);
    return res.status(201).send(parking);
  } catch (error: any) {
    if (error instanceof BadRequest) {
      return res.status(400).send(error);
    }
    return res.status(500).send(error);
  }
};

export const getParking = (parkingService: ParkingService) => async (req: Request, res: Response) => {
  const {
    skip,
    limit,
    order
  } = req.query;

  const payload: SearchInterface = {
    skip: skip ? parseInt(skip.toString()) : undefined,
    limit: limit ? parseInt(limit.toString()) : undefined,
    order: order && order.toString()
  };
  const parkings = await parkingService.getParkings(payload);

  return res.status(200).send(parkings);
};

export const patchParking = (parkingService: ParkingService) => async (req: Request, res: Response) => {
  const { parkingId } = req.params;
  const { contact, spots } = req.body;

  const id = parseInt(parkingId);

  if (isNaN(id)) {
    const error = new Error('Bad request');
    return res.status(400).send(error);
  }

  const payload: Partial<ParkingInput> = {
    contact,
    spots
  };

  try {
    const parkingUpdated = await parkingService.updateParking(id, payload);

    return res.status(200).send(parkingUpdated);
  } catch (error) {
    if (error instanceof BadRequest) {
      return res.status(400).send(error);
    }
    return res.status(500).send(error);
  }
};

import { Router } from 'express';
import { getParking, patchParking, postParking } from './parking';
import { ParkingService } from '../../services/parking.service';
import { validateBody } from '../../middlewares/validate-body.middleware';
import { parkingValidation } from '../../validations/create-parking.validation';
import { patchParkingValidation } from '../../validations/update-parking.validation';
import { authMiddleware } from '../../middlewares/auth.middleware';

export default () => {
  const parkingService = new ParkingService();
  const router = Router();

  router.use(authMiddleware);
  router.get('/', getParking(parkingService));
  router.post('/', validateBody(parkingValidation), postParking(parkingService));
  router.patch('/:parkingId', validateBody(patchParkingValidation), patchParking(parkingService));

  return router;
};

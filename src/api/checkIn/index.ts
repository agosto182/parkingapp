import { Router } from 'express';
import { CheckInService } from '../../services/check-in.service';
import { checkInValidation } from '../../validations/check-in.validation';
import { postCheckIn } from './check-in';
import { validateBody } from '../../middlewares/validate-body.middleware';

export default () => {
  const checkInService = new CheckInService();
  const router = Router();

  router.post('/', validateBody(checkInValidation), postCheckIn(checkInService));

  return router;
};

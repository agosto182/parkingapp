import { Request, Response } from 'express';
import { CheckInService } from '../../services/check-in.service';
import { TicketInputInterface } from '../../interfaces/ticket-input.interface';
import { BadRequest } from '../../errors';

export const postCheckIn = (checkInService: CheckInService) => async (req: Request, res: Response) => {
  const { parkingId, userType } = req.body;

  const payload: TicketInputInterface = {
    parkingId: parseInt(parkingId as string),
    userType: userType as string
  };

  try {
    const ticket = await checkInService.checkIn(payload);

    return res.status(201).send({
      status: 'success',
      ticketId: ticket.id
    });
  } catch (error) {
    if (error instanceof BadRequest) {
      return res.status(400).send(error);
    }
    return res.status(500).send(error);
  }
};

import { Sequelize } from 'sequelize-typescript';
import { getConfig } from './config';
import { models } from './models';

export function connectDatabase () {
  const port = parseInt(getConfig('DB_PORT') || '5432');

  const sequelize = new Sequelize({
    dialect: 'postgres',
    host: getConfig('DB_HOST'),
    port,
    database: getConfig('DB_NAME'),
    username: getConfig('DB_USER'),
    password: getConfig('DB_PASSWORD'),
    models: Object.values(models),
    logging: false
  });

  sequelize.sync();

  return sequelize;
}

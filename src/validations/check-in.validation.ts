import Joi from 'joi';

export const checkInValidation = Joi.object({
  parkingId: Joi.number().required(),
  userType: Joi.string().valid('visitor', 'provider', 'corporate')
});

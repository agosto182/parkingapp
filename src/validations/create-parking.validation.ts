import Joi from 'joi';

export const parkingValidation = Joi.object({
  name: Joi.string().required(),
  spots: Joi.number().min(50).max(1500).required(),
  contact: Joi.string().required(),
  parkingType: Joi.string().valid('public', 'private', 'courtesy')
});

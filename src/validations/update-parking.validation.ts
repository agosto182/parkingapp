import Joi from 'joi';

export const patchParkingValidation = Joi.object({
  spots: Joi.number().min(50).max(1500).required(),
  contact: Joi.string().required()
});

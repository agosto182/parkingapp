import { models } from '../models';

export class TicketStore {
  private model = models.Ticket;

  async create (parkingId: number, userType: string) {
    const ticket = this.model.build({
      parkingId,
      userType
    });

    await ticket.save();

    return ticket;
  }
}

import { models } from '../models';

export class ParkingStore {
  private model = models.Parking;

  async create (name: string, spots: number, contact: string, parkingType: string) {
    const parking = this.model.build({
      name,
      spots,
      contact,
      parkingType
    });

    await parking.save();

    return parking;
  }

  async getById (id: number) {
    const parking = await this.model.findOne({ where: { id } });

    return parking;
  }

  async getPaginated (skip = 0, limit = 100, order = 'id') {
    const parkings = await this.model.findAndCountAll({
      offset: skip,
      limit,
      order: [order]
    });

    return parkings;
  }

  async update (id: number, contact: string, spots: number) {
    const parking = await this.getById(id);

    parking?.set({
      contact,
      spots
    });

    await parking?.save();

    return parking;
  }
}

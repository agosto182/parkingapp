import { BadRequest } from '../errors';
import { models } from '../models';

export class UserStore {
  private model = models.User;

  async create (email: string, password: string) {
    const user = this.model.build({
      email,
      password
    });

    await user.save();

    return user;
  }

  async getByEmail (email: string) {
    const user = await this.model.findOne({ where: { email } });

    if (!user) {
      throw new BadRequest('UserNotFound');
    }

    return user;
  }
}

import { Parking } from './parking.model';
import { Ticket } from './ticket.model';
import { User } from './user.model';

export const models = {
  Parking,
  Ticket,
  User
};

import { AutoIncrement, Column, CreatedAt, DataType, Max, Min, Model, PrimaryKey, Table, Unique, UpdatedAt } from 'sequelize-typescript';

@Table
export class Parking extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
    id: number;

  @Unique
  @Column
    name: string;

  @Min(50)
  @Max(1500)
  @Column
    spots: number;

  @Column
    contact: string;

  @Column({
    type: DataType.ENUM('public', 'private', 'courtesy')
  })
    parkingType: string;

  @CreatedAt
    createdAt: Date;

  @UpdatedAt
    updatedAt: Date;
}

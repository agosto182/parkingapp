import { AutoIncrement, Column, CreatedAt, DataType, ForeignKey, Model, PrimaryKey, Table, UpdatedAt } from 'sequelize-typescript';
import { Parking } from './parking.model';

@Table
export class Ticket extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
    id: number;

  @ForeignKey(() => Parking)
  @Column
    parkingId: string;

  @Column({
    type: DataType.ENUM('visitor', 'provider', 'corporate')
  })
    userType: string;

  @CreatedAt
    createdAt: Date;

  @UpdatedAt
    updatedAt: Date;
}

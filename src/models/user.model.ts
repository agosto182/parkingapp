import { AutoIncrement, Column, CreatedAt, IsEmail, Model, PrimaryKey, Table, UpdatedAt } from 'sequelize-typescript';

@Table
export class User extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
    id: number;

  @IsEmail
  @Column
    email: string;

  @Column
    password: string;

  @CreatedAt
    createdAt: Date;

  @UpdatedAt
    updatedAt: Date;
}

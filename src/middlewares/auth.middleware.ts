import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { getConfig } from '../config';
import { BadRequest } from '../errors';

export const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const secret = getConfig('JWT_SECRET', 'secret') as string;
  const token = req.headers.authorization as string;

  if (!token) {
    res.status(401).send(new BadRequest('UNAUTHORIZED'));
  }

  try {
    jwt.verify(token, secret);

    return next();
  } catch (err) {
    res.status(401).send(new BadRequest('UNAUTHORIZED'));
  }
};

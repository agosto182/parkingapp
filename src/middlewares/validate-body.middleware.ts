import { NextFunction, Request, Response } from 'express';
import Joi from 'joi';

export const validateBody = (schema: Joi.Schema) => async (req: Request, res: Response, next: NextFunction) => {
  const { body } = req;

  try {
    await schema.validateAsync(body);
    next();
  } catch (err) {
    if (err instanceof Joi.ValidationError) {
      return res.status(400).send({
        code: err.name,
        message: err.message
      });
    }

    return res.status(500).send(new Error('Unexpected server error'));
  }
};

export class UnexpectedError extends Error {
  constructor (
    private code: string = 'UNEXPECTED_ERROR'
  ) {
    super();
    this.message = 'An unexpected server error has ocurred';
  }
}

export class BadRequest extends Error {
  constructor (
    private code: string = 'BAD_REQUEST',
    message: string = 'Bad request as sent'
  ) {
    super();
    this.message = message;
  }
}

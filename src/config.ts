interface configInterfce {
  [key: string]: string
}

const env = process.env;

const config: configInterfce = {
  DB_HOST: env.DB_HOST || 'localhost',
  DB_PORT: env.DB_PORT || '5432',
  DB_NAME: env.DB_NAME || 'parkingApp',
  DB_USER: env.DB_USER || 'postgres',
  DB_PASSWORD: env.DB_PASSWORD || 'postgres'
};

export const getConfig = (name: string, defaultValue?: string | undefined) => {
  const value: string | undefined = config[name] || defaultValue;
  return value;
};

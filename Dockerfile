FROM node:16 as base

WORKDIR /app

COPY ./package.json /app/

RUN cd /app
RUN npm install

CMD ["ls"]

FROM node:16 as backend

WORKDIR /app

COPY --from=base /app /app

RUN cd /app

EXPOSE 3000

CMD ["npm", "run", "start:dev"]
